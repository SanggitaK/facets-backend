'use strict';

const Hapi = require('hapi');
import * as route from './file.routes';

const server = new Hapi.Server();
server.connection({ port: 3000 });
server.route(route.routes);

server.start((err) => {
    if (err){
        throw err;
    }
    console.log(`Server running at ${server.info.uri}`);
});