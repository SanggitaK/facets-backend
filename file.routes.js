
export const routes = [
    {
        method: 'GET',
        path: '/healthcheck',
        handler: function (request, reply) {
            reply('Hello World!');
        }
    }
];
